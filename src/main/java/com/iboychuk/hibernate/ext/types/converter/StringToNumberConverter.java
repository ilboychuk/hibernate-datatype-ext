package com.iboychuk.hibernate.ext.types.converter;

//import org.springframework.util.NumberUtils;


public class StringToNumberConverter<T extends Number> implements Converter<T> {

    private final Class<T> targetType;

    public StringToNumberConverter(Class<T> targetType) {
        this.targetType = targetType;
    }

    @Override
    public T convert(String source) {
//        return (source == null || source.isEmpty()) ? null : NumberUtils.parseNumber(source, this.targetType);
        return null;
    }
}
