package com.iboychuk.hibernate.ext.types;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.DynamicParameterizedType;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class HStoreType implements UserType, DynamicParameterizedType {

    private static final int[] SQL_TYPES = new int[]{Types.OTHER};

    public static final String TYPE_NAME = "hstore";

    private HStoreConverter converter;

    @Override
    public int[] sqlTypes() {
        return SQL_TYPES;
    }

    @Override
    public Class returnedClass() {
        return converter.getObjectType();
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return Objects.equals(x, y);
    }

    @Override
    public int hashCode(Object o) throws HibernateException {
        return o.hashCode();
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Object deepCopy(Object o) throws HibernateException {
        return (o == null) ? null : converter.getObjectCopy(o);
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return deepCopy(original);
    }

    @Override
    public Serializable disassemble(Object o) throws HibernateException {
        return (o == null) ? null : (Serializable) deepCopy(o);
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
        String rsString = rs.getString(names[0]);
        return (rsString == null) ? null : converter.toMap(rsString);
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object o, int index, SessionImplementor session) throws HibernateException, SQLException {
        if (o == null) {
            st.setNull(index, Types.OTHER);
        } else {
            st.setObject(index, converter.toStore((Map) o), Types.OTHER);
        }
    }

    @Override
    public void setParameterValues(Properties parameters) {
        try {
            Class entity = Class.forName(parameters.getProperty(ENTITY));
            Field property = entity.getDeclaredField(parameters.getProperty(PROPERTY));

            this.converter = new HStoreConverter(property);

        } catch (NoSuchFieldException | ClassNotFoundException e) {
            throw new IllegalArgumentException("Property lookup failed", e);
        }
    }


}
