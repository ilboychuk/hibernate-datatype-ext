package com.iboychuk.hibernate.ext.types.factory;

import java.util.*;

public class CollectionFactory {

    public static Class<? extends Collection> approximateCollectionClass(Class<?> cls) {
        if (cls.isInterface()) {
            if (Collection.class == cls || List.class == cls) {
                return ArrayList.class;
            } else if (NavigableSet.class == cls || SortedSet.class == cls) {
                return TreeSet.class;
            } else if (Set.class == cls) {
                return HashSet.class;
            }  else {
                throw new IllegalArgumentException("Unsupported collection interface: " + cls.getName());
            }
        } else if (Collection.class.isAssignableFrom(cls)) {
            return cls.asSubclass(Collection.class);
        } else {
            throw new IllegalArgumentException("Class not assignable to map");
        }
    }

    public static Class<? extends Map> approximateMapClass(Class<?> cls) {
        if (cls.isInterface()) {
            if (Map.class == cls) {
                return HashMap.class;
            } else if (SortedMap.class == cls || NavigableMap.class == cls) {
                return TreeMap.class;
            } else if (EnumMap.class == cls) {
                return EnumMap.class;
            } else {
                throw new IllegalArgumentException("Unsupported Map interface: " + cls.getName());
            }
        } else if (Map.class.isAssignableFrom(cls)) {
            return cls.asSubclass(Map.class);
        } else {
            throw new IllegalArgumentException("Class not assignable to map");
        }
    }


    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <K, V> Map<K, V> createMap(Map m, Class<?> mapType, Class<?> keyType) {

        Map<K, V> kvMap;
        if (mapType.isInterface()) {
            if (Map.class == mapType) {
                kvMap = new HashMap<>();
            } else if (SortedMap.class == mapType || NavigableMap.class == mapType) {
                kvMap = new TreeMap<K, V>();
            } else {
                throw new IllegalArgumentException("Unsupported Map interface: " + mapType.getName());
            }
        } else if (EnumMap.class == mapType) {
            kvMap = new EnumMap(asEnumType(keyType));
        } else {
            try {
                kvMap = (Map<K, V>) mapType.newInstance();
            } catch (Throwable ex) {
                throw new IllegalArgumentException("Could not instantiate Map type: " + mapType.getName(), ex);
            }
        }

        if (m != null) {
            kvMap.putAll(m);
        }

        return kvMap;
    }


    @SuppressWarnings("rawtypes")
    private static Class<? extends Enum> asEnumType(Class<?> enumType) {
        if (!Enum.class.isAssignableFrom(enumType)) {
            throw new IllegalArgumentException("Supplied type is not an enum: " + enumType.getName());
        }
        return enumType.asSubclass(Enum.class);
    }

}
