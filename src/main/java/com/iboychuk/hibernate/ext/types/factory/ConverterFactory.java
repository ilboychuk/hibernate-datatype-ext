package com.iboychuk.hibernate.ext.types.factory;

import com.iboychuk.hibernate.ext.types.converter.*;

import java.util.UUID;

public class ConverterFactory {

    public static Converter createConverter(Class<?> cls) {
        if (cls == Object.class || String.class.isAssignableFrom(cls)) {
            // no convertation required
            return source -> source;
        }
        if (Number.class.isAssignableFrom(cls)) {
            return new StringToNumberConverter(cls);
        } else if (UUID.class.isAssignableFrom(cls)) {
            return new StringToUUIDConverter();
        } else if (Boolean.class.isAssignableFrom(cls)) {
            return new StringToBooleanConverter();
        } else if (Enum.class.isAssignableFrom(cls)){
            return new StringToEnumConverter( cls.asSubclass(Enum.class));
        }else {
            throw new IllegalArgumentException("Not supported convertion type");
        }
    }

}
