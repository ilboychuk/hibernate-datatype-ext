package com.iboychuk.hibernate.ext.types.converter;


public class StringToBooleanConverter implements Converter<Boolean> {

    public Boolean convert(String source) {
        if (source == null || source.isEmpty()) {
            return null;
        }

        String value = source.trim().toLowerCase();
        if ("true".equals(value)) {
            return Boolean.TRUE;
        } else if ("false".equals(value)) {
            return Boolean.FALSE;
        } else {
            throw new IllegalArgumentException("Invalid boolean value '" + source + "'");
        }
    }

}
