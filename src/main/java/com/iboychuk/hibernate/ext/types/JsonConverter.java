package com.iboychuk.hibernate.ext.types;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.iboychuk.hibernate.ext.types.factory.CollectionFactory;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;

public class JsonConverter {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_NULL);


    private final Class<?> cls;
    private final JavaType type;

    public JsonConverter(Field field) {
        this.cls = field.getType();
        this.type = buildJavaType(field.getGenericType());
    }

    public Class<?> getObjectType() {
        return cls;
    }

    public Object getObjectCopy(Object obj) {
        return toObject(toJson(obj));
    }

    public Object toObject(String json) {
        try {
            return OBJECT_MAPPER.readValue(json.getBytes("UTF-8"), type);
        } catch (IOException e) {
            throw new IllegalArgumentException("Deserialization error", e);
        }
    }

    public String toJson(Object obj) {
        try {
            return OBJECT_MAPPER.writeValueAsString(obj);
        } catch (IOException e) {
            throw new IllegalArgumentException("Serialization error", e);
        }
    }

    /**
     * Utils method to build jackson javatype
     *
     * @param type - generic type
     * @return jackson java type
     */
    private static JavaType buildJavaType(Type type) {
        TypeFactory typeFactory = OBJECT_MAPPER.getTypeFactory();

        if (type instanceof Class<?>) {
            Class<?> rawType = (Class<?>) type;

            if (Map.class.isAssignableFrom(rawType)) {
                return typeFactory.constructRawMapType(CollectionFactory.approximateMapClass(rawType));
            }

            if (Collection.class.isAssignableFrom(rawType)) {
                return typeFactory.constructRawCollectionType(CollectionFactory.approximateCollectionClass(rawType));
            }

            return typeFactory.constructType(type);
        }

        if (type instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) type;

            if (pt.getRawType() instanceof Class) {
                Class<?> rawType = (Class<?>) pt.getRawType();

                if (Map.class.isAssignableFrom(rawType)) {
                    return typeFactory.constructMapType(CollectionFactory.approximateMapClass(rawType),
                            buildJavaType(pt.getActualTypeArguments()[0]), buildJavaType(pt.getActualTypeArguments()[1]));
                }

                if (Collection.class.isAssignableFrom(rawType)) {
                    return typeFactory.constructCollectionType(CollectionFactory.approximateCollectionClass(rawType),
                            buildJavaType(pt.getActualTypeArguments()[0]));
                }
            }

            return buildJavaType(type);
        }

        throw new IllegalArgumentException("Failed to build jackson type for " + type);
    }


}
