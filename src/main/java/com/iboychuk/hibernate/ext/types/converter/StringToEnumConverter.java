package com.iboychuk.hibernate.ext.types.converter;

public class StringToEnumConverter<T extends Enum<T>> implements Converter<T> {

    private final Class<T> enumType;

    public StringToEnumConverter(Class<T> enumType) {
        this.enumType = enumType;
    }

    @Override
    public T convert(String source) {
        return (source.isEmpty()) ? null : Enum.valueOf(this.enumType, source);
    }
}
