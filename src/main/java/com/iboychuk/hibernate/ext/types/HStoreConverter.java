package com.iboychuk.hibernate.ext.types;

import com.iboychuk.hibernate.ext.types.converter.*;
import com.iboychuk.hibernate.ext.types.factory.CollectionFactory;
import com.iboychuk.hibernate.ext.types.factory.ConverterFactory;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

public class HStoreConverter {

    private final Class<?> mapType;
    private final Class<?> keyType;

    private final Converter keyConverter;
    private final Converter valueConverter;

    /**
     * todo: add properties.class support
     */
    public HStoreConverter(Field field) {
        if (Map.class.isAssignableFrom(field.getType())) {
            Type type = field.getGenericType();

            Class<?> keyType = Object.class;
            Class<?> valueType = Object.class;
            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                keyType = (Class<?>) parameterizedType.getActualTypeArguments()[0];
                valueType = (Class<?>) parameterizedType.getActualTypeArguments()[1];
            }

            this.mapType = field.getType();

            this.keyType = keyType;
            this.keyConverter = ConverterFactory.createConverter(keyType);
            this.valueConverter = ConverterFactory.createConverter(valueType);

        } else {
            throw new IllegalArgumentException("Only map types are supported");
        }
    }

    public Class<?> getObjectType() {
        return Map.class;
    }

    public Map getObjectCopy(Object o) {
        return CollectionFactory.createMap((Map) o, mapType, keyType);
    }

    public String toStore(Map<?, ?> map) {
        if (map.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder(map.size() * 8);
        for (Map.Entry<?, ?> e : map.entrySet()) {
            appendEscaped(sb, e.getKey());
            sb.append("=>");
            appendEscaped(sb, e.getValue());
            sb.append(", ");
        }
        sb.setLength(sb.length() - 2);
        return sb.toString();
    }

    public Map toMap(String store) {
        Map<Object, Object> m = CollectionFactory.createMap(null, mapType, keyType);
        int pos = 0;
        StringBuilder sb = new StringBuilder();
        while (pos < store.length()) {
            sb.setLength(0);
            int start = store.indexOf('"', pos);
            int end = appendUntilQuote(sb, store, start);
            String key = sb.toString();
            pos = end + 3;

            String val;
            if (store.charAt(pos) == 'N') {
                val = null;
                pos += 4;
            } else {
                sb.setLength(0);
                end = appendUntilQuote(sb, store, pos);
                val = sb.toString();
                pos = end;
            }
            pos++;

            m.put(keyConverter.convert(key), valueConverter.convert(val));
        }
        return m;
    }


    private static void appendEscaped(StringBuilder sb, Object val) {
        if (val != null) {
            sb.append('"');
            String s = val.toString();

            for (int pos = 0; pos < s.length(); ++pos) {
                char ch = s.charAt(pos);
                if (ch == '"' || ch == '\\') {
                    sb.append('\\');
                }
                sb.append(ch);
            }
            sb.append('"');
        } else {
            sb.append("NULL");
        }
    }

    private static int appendUntilQuote(StringBuilder sb, String s, int pos) {
        for (pos += 1; pos < s.length(); pos++) {
            char ch = s.charAt(pos);
            if (ch == '"') {
                break;
            }
            if (ch == '\\') {
                pos++;
                ch = s.charAt(pos);
            }
            sb.append(ch);
        }
        return pos;
    }


}
