package com.iboychuk.hibernate.ext.types.dialect;

import org.hibernate.dialect.PostgreSQL94Dialect;

import java.sql.Types;

public class ExtendedPostgresDialect extends PostgreSQL94Dialect {

    public ExtendedPostgresDialect() {
        super();
        registerColumnType(Types.OTHER, "jsonb");
        registerColumnType(Types.OTHER, "hstore");
    }
}
