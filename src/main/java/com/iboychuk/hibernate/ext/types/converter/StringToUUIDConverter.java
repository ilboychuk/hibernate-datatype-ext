package com.iboychuk.hibernate.ext.types.converter;

import java.util.UUID;

public class StringToUUIDConverter implements Converter<UUID> {

    @Override
    public UUID convert(String source) {
        return (source == null || source.isEmpty()) ? null : UUID.fromString(source);
    }
}
