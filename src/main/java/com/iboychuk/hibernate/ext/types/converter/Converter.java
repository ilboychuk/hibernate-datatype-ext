package com.iboychuk.hibernate.ext.types.converter;


public interface Converter<R> {
    R convert(String source);
}
