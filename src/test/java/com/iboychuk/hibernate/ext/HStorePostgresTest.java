package com.iboychuk.hibernate.ext;


import com.iboychuk.hibernate.ext.app.hstore.HStoreEntity;
import com.iboychuk.hibernate.ext.app.hstore.HStoreEntityRepository;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.util.Collections.singletonMap;

public class HStorePostgresTest extends AbstractPostgresTest {

    @Autowired
    private HStoreEntityRepository hStoreEntityRepository;

    @Test
    public void sampleTest() throws Exception {
        List<HStoreEntity> entities = hStoreEntityRepository.findAll();
        Assert.assertNotNull(entities);
    }

    @Test
    public void saveNullPropertiesTest() throws Exception {
        HStoreEntity entity = new HStoreEntity();
        hStoreEntityRepository.save(entity);
    }


    // TODO add tests
    @Test
    public void saveAddReadEnumMapTest() throws Exception {

    }

    @Test
    public void saveAddReadUUIDMapTest() throws Exception {

    }

    @Test
    public void saveAddReadTreeMapTest() throws Exception {

    }

    @Test
    public void readBrokenUUIDMapTest() throws Exception {

    }
}
