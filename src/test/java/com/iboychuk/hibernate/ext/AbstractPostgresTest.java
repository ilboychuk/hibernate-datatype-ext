package com.iboychuk.hibernate.ext;

import com.iboychuk.hibernate.ext.app.hstore.HStoreEntity;
import com.iboychuk.hibernate.ext.app.hstore.HStoreEntityRepository;
import com.iboychuk.hibernate.ext.app.json.JsonEntity;
import com.iboychuk.hibernate.ext.app.json.JsonEntityRepository;
import org.hibernate.boot.jaxb.SourceType;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.util.EnvironmentTestUtils;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;

@DataJpaTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = AbstractPostgresTest.Initializer.class,
        classes = {
            JsonEntity.class, JsonEntityRepository.class,
            HStoreEntity.class, HStoreEntityRepository.class
        }
)
@DirtiesContext
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public abstract class AbstractPostgresTest {

    @ClassRule
    public static GenericContainer POSTGRES = new PostgreSQLContainer("postgres:9.5.3")
            .withUsername("test").withPassword("test").withDatabaseName("test")
            .withClasspathResourceMapping("postgres-init.sql", "/docker-entrypoint-initdb.d/postgres-init.sql", BindMode.READ_WRITE);

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            EnvironmentTestUtils.addEnvironment("postgres-container", configurableApplicationContext.getEnvironment(),
                    "spring.jpa.hibernate.ddl-auto=" + "validate",
                    "spring.datasource.username=" + "test",
                    "spring.datasource.password=" + "test",
                    "spring.datasource.url=" + jdbcUrl(),
                    "spring.datasource.driver-class-name=" + "com.p6spy.engine.spy.P6SpyDriver"
            );
        }
    }

    private static String jdbcUrl(){
        return "jdbc:p6spy:postgresql://" + POSTGRES.getContainerIpAddress() + ":" + POSTGRES.getMappedPort(5432) + "/" + "test";
    }
}
