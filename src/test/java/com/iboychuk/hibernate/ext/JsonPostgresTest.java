package com.iboychuk.hibernate.ext;

import com.iboychuk.hibernate.ext.app.json.JsonEntity;
import com.iboychuk.hibernate.ext.app.json.JsonEntityRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;

public class JsonPostgresTest extends AbstractPostgresTest {

    @Autowired
    private JsonEntityRepository jsonEntityRepository;

    @Test
    public void findTest() throws Exception {
        jsonEntityRepository.findAll();
    }

    @Test
    public void saveTest() throws Exception {
        JsonEntity entity = new JsonEntity();

        entity.setUserView(new JsonEntity.JsonUser("Bob", "Skywoker", 12));
        entity.setUserViewMap(singletonMap( 1, new JsonEntity.JsonUser("Ann", "Lice", 55)));
        entity.setUserViewList(singletonList(new JsonEntity.JsonUser("List", "List", 33)));
        entity.setUserViewComplex(singletonMap( 1, singletonList(new JsonEntity.JsonUser("List", "List", 33))));

        entity.setUserParameterMap(singletonMap("sum", 100));
        entity.setUserParameterList(singletonList(23));
        entity.setUserParameterComplex(singletonMap("ddd", singletonList(66)));

        jsonEntityRepository.save(entity);

        List<JsonEntity> all = jsonEntityRepository.findAll();
        System.out.println("Size " + all.size());

        jsonEntityRepository.findAll().forEach(
                System.out::println
        );

    }
}
