package com.iboychuk.hibernate.ext.app.json;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface JsonEntityRepository extends JpaRepository<JsonEntity, Integer> {
}
