package com.iboychuk.hibernate.ext.app.hstore;

import com.iboychuk.hibernate.ext.types.HStoreType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
@Table(name = "hstore_entity")
@TypeDef(name = HStoreType.TYPE_NAME, typeClass = HStoreType.class)
public class HStoreEntity {

    public enum Enumerate {
        ONE, TWO, THREE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Type(type = HStoreType.TYPE_NAME)
    @Column(name = "tree_str_map", columnDefinition = "hstore")
    private TreeMap<String, String> treeStringMap;

    @Type(type = HStoreType.TYPE_NAME)
    @Column(name = "hash_int_map", columnDefinition = "hstore")
    private HashMap<Integer, Integer> hashIntMap;

    @Type(type = HStoreType.TYPE_NAME)
    @Column(name = "enum_map", columnDefinition = "hstore")
    private EnumMap<Enumerate, String> enumMap;

    @Type(type = HStoreType.TYPE_NAME)
    @Column(name = "uuid_map", columnDefinition = "hstore")
    private Map<UUID, Long> uuidMap;

    /*
     * Row type don't work.
     *
     * @Type(type = HStoreType.TYPE_NAME)
     * @Column(name = "row_map", columnDefinition = "hstore")
     * private Map rowMap;
     *
     *
     * Spring method below throws NullPointerException
     *
     * org.springframework.data.mapping.model.AbstractPersistentProperty
     *
     * public Class<?> getActualType() {
     *      return this.information.getActualType().getType();
     * }
     *
     */



}
