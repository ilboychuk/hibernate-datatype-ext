package com.iboychuk.hibernate.ext.app;

import com.p6spy.engine.spy.appender.MessageFormattingStrategy;
import org.hibernate.engine.jdbc.internal.BasicFormatterImpl;
import org.hibernate.engine.jdbc.internal.Formatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SpyMessageFormatter implements MessageFormattingStrategy {
    private final static Formatter SQL_FORMATTER = new BasicFormatterImpl();
    private final static DateFormat DATE_FORMATTER = new SimpleDateFormat("HH:mm:ss.SSS");

    @Override
    public String formatMessage(int connectionId, String now, long elapsed, String category, String prepared, String sql) {
        return "\n -> id: " + connectionId + " (" + category + ") | " + DATE_FORMATTER.format(new Date(Long.valueOf(now))) + " (" + elapsed + "ms)" + "\n -> " + sql;
    }
}
