package com.iboychuk.hibernate.ext.app.json;

import com.iboychuk.hibernate.ext.types.JsonType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;
import java.util.Map;


@Data
@Entity
@Table(name = "json_entity")
@TypeDef(name = JsonType.BINARY_TYPE_NAME, typeClass = JsonType.class)
public class JsonEntity {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JsonUser {
        private String name;
        private String surname;
        private Integer age;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Type(type = JsonType.BINARY_TYPE_NAME)
    @Column(name = "user_view", columnDefinition = "jsonb")
    private JsonUser userView;

    @Type(type = JsonType.BINARY_TYPE_NAME)
    @Column(name = "user_view_map", columnDefinition = "jsonb")
    private Map<Integer, JsonUser> userViewMap;

    @Type(type = JsonType.BINARY_TYPE_NAME)
    @Column(name = "user_view_list", columnDefinition = "jsonb")
    private List<JsonUser> userViewList;

    @Type(type = JsonType.BINARY_TYPE_NAME)
    @Column(name = "user_view_complex", columnDefinition = "jsonb")
    private Map<Integer, List<JsonUser>> userViewComplex;

    @Type(type = JsonType.BINARY_TYPE_NAME)
    @Column(name = "user_parameter_map", columnDefinition = "jsonb")
    private Map<String, Integer> userParameterMap;

    @Type(type = JsonType.BINARY_TYPE_NAME)
    @Column(name = "user_parameter_list", columnDefinition = "jsonb")
    private List<Integer> userParameterList;

    @Type(type = JsonType.BINARY_TYPE_NAME)
    @Column(name = "user_parameter_complex", columnDefinition = "jsonb")
    private Map<String, List<Integer>> userParameterComplex;

}
