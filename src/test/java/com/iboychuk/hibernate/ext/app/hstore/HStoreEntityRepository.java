package com.iboychuk.hibernate.ext.app.hstore;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HStoreEntityRepository extends JpaRepository<HStoreEntity, Integer> {
}
