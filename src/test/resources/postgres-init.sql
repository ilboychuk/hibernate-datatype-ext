
create extension if not exists "hstore" schema pg_catalog;

CREATE TABLE json_entity (
    id                  SERIAL PRIMARY KEY,
    user_view           JSONB,
    user_view_map       JSONB,
    user_view_list      JSONB,
    user_view_complex   JSONB,

    user_parameter_map      JSONB,
    user_parameter_list     JSONB,
    user_parameter_complex  JSONB
);

CREATE TABLE hstore_entity (
    id              SERIAL PRIMARY KEY,
    tree_str_map    HSTORE,
    hash_int_map    HSTORE,
    enum_map        HSTORE,
    uuid_map        HSTORE
);

insert into hstore_entity (hash_int_map) values ('1=>1,2=>2,3=>3');